---
aliases: 
tags: 
---

# Readme

## 220118 - SiO2 Patching

This update brings my version of the fabrication file in sync with Robin's. It also provides SiO2 patches beneath each Nb upper probe, as the primary goal. I've done some refactoring while here, as well:

- New naming convention - see [[cell hierarchy]].
- More cells within cells, including:
	 - `TC` within `TC_B`
	 - `PRBE` Probe cells for both `TOP` (SiO2 and tapered probe) geometry, and `BTM` (plug/etch and tapered probe) geometry.
- Standardized probe and penetration depth across all chip architectures
	 - Added rectangles at base electrode interface, for easy alignment
	 - `TC` still has its own unique probe, but at least the penetration depths are synced.
